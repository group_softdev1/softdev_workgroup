/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.UI;

import com.werapan.databaseproject.model.Employee;
import com.werapan.databaseproject.model.Promotion;
import com.werapan.databaseproject.service.PromotionService;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bbnpo
 */
public class PromotionTable extends javax.swing.JPanel {

    /**
     * Creates new form PromotionTable
     */
    private PromotionService promotionService;
    private List<Promotion> list;
    private Promotion editedPromotion;
 private Employee loggedInEmployee;
    public PromotionTable(Employee loggedInEmployee) {
        initComponents();
        this.loggedInEmployee = loggedInEmployee;
        promotionService = new PromotionService();
        list = promotionService.getPromotions();
        tblPromotionTable.setModel(new AbstractTableModel() {
            String[] colNames = {"ID", "Condition", "Name", "Status", "Date", "ExpDate"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Promotion promotion = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return promotion.getId();
                    case 1:
                        return promotion.getCondition();
                    case 2:
                        return promotion.getName();
                    case 3:
                        return promotion.getStatus();
                    case 4:
                        return promotion.getStartDate();
                    case 5:
                        return promotion.getExpDate();
                    default:
                        return "Unknown";
                }
            }
        });
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblPromotionTable = new javax.swing.JTable();
        btnMainMenu = new javax.swing.JButton();

        tblPromotionTable.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblPromotionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblPromotionTable);

        btnMainMenu.setBackground(new java.awt.Color(104, 224, 145));
        btnMainMenu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnMainMenu.setText("Close");
        btnMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainMenuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMainMenu)
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnMainMenu)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnMainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMainMenuActionPerformed
PointOfSell pointOfSell = new PointOfSell(loggedInEmployee);

        // Get the current JFrame
        JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(this);

        // Replace the current panel (MemberPanel) with the MenuPanel
        frame.setContentPane(pointOfSell);
        frame.revalidate();
        frame.repaint();


    }//GEN-LAST:event_btnMainMenuActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnMainMenu;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPromotionTable;
    // End of variables declaration//GEN-END:variables
}
