/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject;

import com.werapan.databaseproject.dao.BillDao;
import com.werapan.databaseproject.model.Bill;

/**
 *
 * @author waran
 */
public class TestBillDao {

    public static void main(String[] args) {
        BillDao billDao = new BillDao();
//        System.out.println(billDao.getAll());
        for (Bill u : billDao.getAll()) {
            System.out.println("bill = "+u);
        }
    }
}
