/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.mycompany.UI.ProductUi;
import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author J
 */
public class ProductService {

    private ArrayList<Product> productList;
    //private int lastId = 1;
    private ProductDao productDao = new ProductDao();
    
    public List<Product> getProduct(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" PRODUCT_NAME asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);     
    }
    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" PRODUCT_NAME ASC ");
    }

    public ArrayList<Product> getProductsByID() {
        return (ArrayList<Product>) productDao.getAll(" ID_PRODUCT ASC ");
    }
    
    public Product getProductsByID(int i) {
        return productDao.get(i);
    }

    public ProductService() {
        productList = new ArrayList<Product>();
    }

//    public Product addProduct(Product newProduct) {
//        newProduct.setId(lastId++);
//        productList.add(newProduct);
//        return newProduct;
//    }

    public Product addProduct(int index) {
        return productList.get(index);
    }

    public ArrayList<Product> getProducts() {
        return productList;
    }

    public int getSize() {
        return productList.size();
    }

    public void logProductList() {
        for (Product u : productList) {
            System.out.println(u);
        }
    }
}
