/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.CateDao;
import com.werapan.databaseproject.model.Category;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CategoryService {
//    public Category cate(int id) {
//        CateDao cateDao = new CateDao();
//        Category category = cateDao.get(id);
//        if(category != null) {
//            return category;
//        }
//        return null;
//    }
    
    public List<Category> getCategory(){
        CateDao cateDao = new CateDao();
        return cateDao.getAll(" CATE_NAME asc");
    }

    public Category addNew(Category editedCate) {
        CateDao cateDao = new CateDao();
        return cateDao.save(editedCate);
    }

    public Category update(Category editedCate) {
        CateDao cateDao = new CateDao();
        return cateDao.update(editedCate);    
    }

    public int delete(Category editedCate) {
        CateDao cateDao = new CateDao();
        return cateDao.delete(editedCate);     
    }
}
