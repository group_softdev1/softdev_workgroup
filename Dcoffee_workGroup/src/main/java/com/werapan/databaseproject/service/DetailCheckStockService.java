/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.DetailCheckStockDao;
import com.werapan.databaseproject.model.DetailCheckStock;
import java.util.List;

/**
 *
 * @author ACER
 */
public class DetailCheckStockService {
    public List<DetailCheckStock> getDetailCheckStock(){
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.getAll(" ID_DCS asc");
    }

    public DetailCheckStock addNew(DetailCheckStock editedDetailCheckStock) {
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.save(editedDetailCheckStock);
    }

    public DetailCheckStock update(DetailCheckStock editedDetailCheckStock) {
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.update(editedDetailCheckStock);    
    }

    public int delete(DetailCheckStock editedDetailCheckStock) {
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.delete(editedDetailCheckStock);     
    }
    
    public List<DetailCheckStock> getDetailCheckStockDao(){
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.getDetailCheckStockDao();
    }
    
    public List<DetailCheckStock> getAllSS(){
        DetailCheckStockDao detailCheckStockDao = new DetailCheckStockDao();
        return detailCheckStockDao.getAllSS();
    }
}
