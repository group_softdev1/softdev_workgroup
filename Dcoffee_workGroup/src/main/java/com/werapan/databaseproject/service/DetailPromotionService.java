/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.DetailPromotionDao;
import com.werapan.databaseproject.model.DetailPromotion;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class DetailPromotionService {
     public List< DetailPromotion> getDetailCheckStock() {
         DetailPromotionDao  detailPromotionDao = new  DetailPromotionDao();
        return  detailPromotionDao.getAll("ID_DEPROM asc");
    }

    public DetailPromotion addNew(DetailPromotion editedDetailPromotion) {
        DetailPromotionDao detailPromotionDao = new DetailPromotionDao();
        return detailPromotionDao.save(editedDetailPromotion);
    }

    public DetailPromotion update(DetailPromotion editedDetailPromotion) {
        DetailPromotionDao detailPromotionDao = new DetailPromotionDao();
        return detailPromotionDao.update(editedDetailPromotion);
    }

    public int deleteupdate(DetailPromotion editedDetailPromotion) {
        DetailPromotionDao detailPromotionDao = new DetailPromotionDao();
        return detailPromotionDao.delete(editedDetailPromotion);
    }
    
}
