/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.DetailBuyIngredientDao;
import com.werapan.databaseproject.dao.EmployeeDao;
import com.werapan.databaseproject.model.DetailBuyIngredientModel;
import com.werapan.databaseproject.model.Employee;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class DetailBuyIngredientService {
    public List<DetailBuyIngredientModel> getDetailBuyIngredient(){
        DetailBuyIngredientDao detailBuyIngredientDao = new DetailBuyIngredientDao();
        return detailBuyIngredientDao.getAll(" ID_BI asc");
    }
    public DetailBuyIngredientModel addNew(DetailBuyIngredientModel editedDetailBuyIngredient) {
        DetailBuyIngredientDao detailBuyIngredientDao = new DetailBuyIngredientDao();
        return detailBuyIngredientDao.save(editedDetailBuyIngredient);
    }

    public DetailBuyIngredientModel update(DetailBuyIngredientModel editedDetailBuyIngredient) {
        DetailBuyIngredientDao detailBuyIngredientDao = new DetailBuyIngredientDao();
        return detailBuyIngredientDao.update(editedDetailBuyIngredient);    
    }

    public int delete(DetailBuyIngredientModel editedDetailBuyIngredient) {
        DetailBuyIngredientDao detailBuyIngredientDao = new DetailBuyIngredientDao();
        return detailBuyIngredientDao.delete(editedDetailBuyIngredient);     
    }
    
    
}
