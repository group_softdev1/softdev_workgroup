/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.SalaryDao;
import com.werapan.databaseproject.model.Salary;
import java.util.List;

/**
 *
 * @author ACER
 */
public class SalaryService {
    public List<Salary> getSalary(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(" ID_SPD asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);    
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);     
    }
}
