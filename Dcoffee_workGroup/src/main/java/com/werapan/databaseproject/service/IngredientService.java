/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.IngredientDao;
import com.werapan.databaseproject.model.Ingredient;
import java.util.List;

/**
 *
 * @author J
 */
public class IngredientService {
    
    public Ingredient getById(int id) {
        IngredientDao ingredientDao = new IngredientDao();
        Ingredient ingredient = ingredientDao.get(id);
            return ingredient;
    }
    public List<Ingredient> getIngredients(){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll(" ID_IGD asc");
    }

    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }

    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);    
    }

    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);     
    }
}
