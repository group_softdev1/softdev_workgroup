/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.CheckStockIngredientDao;
import com.werapan.databaseproject.model.CheckStockIngredient;
import java.util.List;

/**
 *
 * @author ACER
 */
public class CheckStockIngredientService {
    
    public List<CheckStockIngredient> getCheckStockIngredient(){
        CheckStockIngredientDao checkStockIngredientDao = new CheckStockIngredientDao();
        return checkStockIngredientDao.getAll(" ID_CS asc");
    }

    public CheckStockIngredient addNew(CheckStockIngredient editedCheckStockIngredient) {
        CheckStockIngredientDao checkStockIngredientDao = new CheckStockIngredientDao();
        return checkStockIngredientDao.save(editedCheckStockIngredient);
    }

    public CheckStockIngredient update(CheckStockIngredient editedCheckStockIngredient) {
        CheckStockIngredientDao checkStockIngredientDao = new CheckStockIngredientDao();
        return checkStockIngredientDao.update(editedCheckStockIngredient);    
    }

    public int delete(CheckStockIngredient editedCheckStockIngredient) {
        CheckStockIngredientDao checkStockIngredientDao = new CheckStockIngredientDao();
        return checkStockIngredientDao.delete(editedCheckStockIngredient);     
    }
}
