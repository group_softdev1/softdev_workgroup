/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;


import com.werapan.databaseproject.dao.WorkingHoursDao;
import com.werapan.databaseproject.model.WorkingHours;
import java.util.List;

/**
 *
 * @author ACER
 */
public class WorkingHoursService {
    public List<WorkingHours> getWorkingHours(){
        WorkingHoursDao workingHoursDao = new WorkingHoursDao();
        return workingHoursDao.getAll(" ID_WORKINGHOURS asc");
    }

    public WorkingHours addNew(WorkingHours editedWorkingHours) {
        WorkingHoursDao workingHoursDao = new WorkingHoursDao();
        return workingHoursDao.save(editedWorkingHours);
    }

    public WorkingHours update(WorkingHours editedWorkingHours) {
        WorkingHoursDao workingHoursDao = new WorkingHoursDao();
        return workingHoursDao.update(editedWorkingHours);    
    }

    public int delete(WorkingHours editedWorkingHours) {
        WorkingHoursDao workingHoursDao = new WorkingHoursDao();
        return workingHoursDao.delete(editedWorkingHours);     
    }
}
