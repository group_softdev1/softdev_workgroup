/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.PromotionDao;
import com.werapan.databaseproject.model.Promotion;
import java.util.List;

/**
 *
 * @author werapan
 */
public class PromotionService {
//    public Promotion getByCondition(String condition) {
//        PromotionDao promotionDao = new PromotionDao();
//        Promotion promotion = promotionDao.getByCondition(condition);
//        return promotion;
//        }
    
//    public Promotion getPromotion(){
//        PromotionDao promotionDao = new PromotionDao();
//        return promotionDao.getAll("ID_PROM asc");
//        }
    
    public List<Promotion> getPromotions(){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" ID_PROM asc");
    }

    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);    
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);     
    }
}
