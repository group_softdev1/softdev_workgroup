/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;


import com.werapan.databaseproject.dao.BranchDao;
import com.werapan.databaseproject.model.Branch;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BranchService  {
   public List<Branch> getBranch(){
        BranchDao branchDao = new BranchDao();
        return branchDao.getAll(" ID_BRANCH asc");
    }

    public Branch addNew(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.save(editedBranch);
    }

    public Branch update(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.update(editedBranch);    
    }

    public int delete(Branch editedBranch) {
        BranchDao branchDao = new BranchDao();
        return branchDao.delete(editedBranch);     
    }
    
}
