/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.BillDao;
import com.werapan.databaseproject.model.Bill;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BillService {
    public List<Bill> getBill(){
        BillDao billDao = new BillDao();
        return billDao.getAll(" ID_BILL asc");
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.save(editedBill);
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);    
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);     
    }
}
