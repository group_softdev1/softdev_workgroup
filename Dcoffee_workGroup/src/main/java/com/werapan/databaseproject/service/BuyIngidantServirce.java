/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;



import com.werapan.databaseproject.dao.BuyInGredientsDao;
import com.werapan.databaseproject.model.BuyInGredients;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BuyIngidantServirce {
   public List<BuyInGredients> getBuyInGredients(){
        BuyInGredientsDao buyInGredientsDao = new BuyInGredientsDao();
        return buyInGredientsDao.getAll(" ID_DBI asc");
    }

    public BuyInGredients addNew(BuyInGredients editedBuyInGredients) {
        BuyInGredientsDao buyInGredientsDao = new BuyInGredientsDao();
        return buyInGredientsDao.save(editedBuyInGredients);
    }

    public BuyInGredients update(BuyInGredients editedBuyInGredients) {
        BuyInGredientsDao buyInGredientsDao = new BuyInGredientsDao();
        return buyInGredientsDao.update(editedBuyInGredients);    
    }

    public int delete(BuyInGredients editedBuyInGredients) {
        BuyInGredientsDao buyInGredientsDao = new BuyInGredientsDao();
        return buyInGredientsDao.delete(editedBuyInGredients);     
    }
    
}
