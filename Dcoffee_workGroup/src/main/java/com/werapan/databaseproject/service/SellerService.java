/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.SellerDao;
import com.werapan.databaseproject.model.Seller;
import java.util.List;

/**
 *
 * @author Murphy
 */
public class SellerService {

    public List<Seller> getSellers(){
        SellerDao sellerDao = new SellerDao();
        return sellerDao.getAll(" ID_SEL asc");
    }

    public Seller addNew(Seller editedSeller) {
        SellerDao sellerDao = new SellerDao();
        return sellerDao.save(editedSeller);
    }

    public Seller update(Seller editedSeller) {
        SellerDao sellerDao = new SellerDao();
        return sellerDao.update(editedSeller);    
    }

    public int delete(Seller editedSeller) {
        SellerDao sellerDao = new SellerDao();
        return sellerDao.delete(editedSeller);     
    }
    
}
