/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject;

import com.werapan.databaseproject.dao.EmployeeDao;
import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Employee;

/**
 *
 * @author werapan
 */
public class TestEmployeeDao {
    public static void main(String[] args) {
        EmployeeDao userDao = new EmployeeDao();
        for(Employee u: userDao.getAll()) {
            System.out.println(u);
        }
//        Employee employee1 = employeeDao.get(2);
//        System.out.println(employee1);
        
//        Employee newEmployee = new Employee("user3", "password", 2, "F");
//        Employee insertedEmployee = userDao.save(newEmployee);
//        System.out.println(insertedEmployee);
//        insertedEmployee.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        Employee updateEmployee = userDao.get(user1.getId());
//        System.out.println(updateEmployee);
//        
//        userDao.delete(user1);
//        for(Employee u: userDao.getAll()) {
//            System.out.println(u);
//        }
//        
//        
//        for(Employee u: userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
//            System.out.println(u);
//        }
        
        DatabaseHelper.close();
    }
}
