/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class DetailCheckStock {
    private int id;
    private int idCS;
    private int idIGD;
    private String name;
    private int QOriginal;
    private int QCurrent;

    public DetailCheckStock(int id, int idCS, int idIGD, String name, int QOriginal, int QCurrent) {
        this.id = id;
        this.idCS = idCS;
        this.idIGD = idIGD;
        this.name = name;
        this.QOriginal = QOriginal;
        this.QCurrent = QCurrent;
    }

    public DetailCheckStock(int idCS, int idIGD, String name, int QOriginal, int QCurrent) {
        this.id = -1 ;
        this.idCS = idCS;
        this.idIGD = idIGD;
        this.name = name;
        this.QOriginal = QOriginal;
        this.QCurrent = QCurrent;
    }

    public DetailCheckStock() {
        this.id = -1 ;
        this.idCS = 0;
        this.idIGD = 0;
        this.name = "";
        this.QOriginal = 0;
        this.QCurrent = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCS() {
        return idCS;
    }

    public void setIdCS(int idCS) {
        this.idCS = idCS;
    }

    public int getIdIGD() {
        return idIGD;
    }

    public void setIdIGD(int idIGD) {
        this.idIGD = idIGD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQOriginal() {
        return QOriginal;
    }

    public void setQOriginal(int QOriginal) {
        this.QOriginal = QOriginal;
    }

    public int getQCurrent() {
        return QCurrent;
    }

    public void setQCurrent(int QCurrent) {
        this.QCurrent = QCurrent;
    }

    @Override
    public String toString() {
        return "DetailCheckStock{" + "id=" + id + ", idCS=" + idCS + ", idIGD=" + idIGD + ", name=" + name + ", QOriginal=" + QOriginal + ", QCurrent=" + QCurrent + '}';
    }
    
    public static DetailCheckStock fromRS(ResultSet rs) {
        DetailCheckStock detailCheckStock = new DetailCheckStock();
        try {
            detailCheckStock.setId(rs.getInt("ID_DCS"));
            detailCheckStock.setIdCS(rs.getInt("ID_CS"));
            detailCheckStock.setIdIGD(rs.getInt("ID_IGD"));
            detailCheckStock.setName(rs.getString("DCS_NAME"));
            detailCheckStock.setQOriginal(rs.getInt("DCS_QUANTITY_ORIGINAL"));
            detailCheckStock.setQCurrent(rs.getInt("DCS_QUANTITY_CURRENT"));
        } catch (SQLException ex) {
            Logger.getLogger(DetailCheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return detailCheckStock;
    }
    
    
}
