/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class Promotion {

    private int id;
    private String condition;
    private String name;
    private String status;
    private String startDate;
    private String expDate;

    public Promotion(int id, String name, String status, String startDate, String expDate) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.startDate = startDate;
        this.expDate = expDate;
    }

    public Promotion(String name, String status, String startDate, String expDate) {
        this.id = -1;
        this.name = name;
        this.status = status;
        this.startDate = startDate;
        this.expDate = expDate;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.status = "No";
        this.startDate = "";
        this.expDate = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", condition=" + condition + ", name=" + name + ", status=" + status + ", startDate=" + startDate + ", expDate=" + expDate + '}';
    }
    
    

    

    public static Promotion fromRS(ResultSet rs){
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("ID_PROM"));
            promotion.setCondition(rs.getString("PROM_CONDITION"));
            promotion.setName(rs.getString("PROM_NAME"));
            promotion.setStatus(rs.getString("PROM_STATUS"));
            promotion.setStartDate(rs.getString("PROM_START_DATE"));
            promotion.setExpDate(rs.getString("PROM_EXP_DATE"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}
