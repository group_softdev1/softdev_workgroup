/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class BuyInGredients {

    private static Object ex;
    private int id;
    private int idBI;
    private int idEmployee;
    private int sel;
    private String oderDate;
    private String receieveDate;
    private int totalUnit;
    private double totalPrice;
    private double netAmount;

    public BuyInGredients(int id, int idBI, int idEmployee, int sel, String oderDate, String receieveDate, int totalUnit, double totalPrice, double netAmount) {
        this.id = id;
        this.idBI = idBI;
        this.idEmployee = idEmployee;
        this.sel = sel;
        this.oderDate = oderDate;
        this.receieveDate = receieveDate;
        this.totalUnit = totalUnit;
        this.totalPrice = totalPrice;
        this.netAmount = netAmount;
    }

    public BuyInGredients(int idBI, int idEmployee, int sel, String oderDate, String receieveDate, int totalUnit, double totalPrice, double netAmount) {
        this.id = -1;
        this.idBI = idBI;
        this.idEmployee = idEmployee;
        this.sel = sel;
        this.oderDate = oderDate;
        this.receieveDate = receieveDate;
        this.totalUnit = totalUnit;
        this.totalPrice = totalPrice;
        this.netAmount = netAmount;
    }

    public BuyInGredients() {
        this.id = -1;
        this.idBI = 0;
        this.idEmployee = 0;
        this.sel = 0;
        this.oderDate = "";
        this.receieveDate = "";
        this.totalUnit = 0;
        this.totalPrice = 0;
        this.netAmount = 0;

    }

    public static Object getEx() {
        return ex;
    }

    public static void setEx(Object ex) {
        BuyInGredients.ex = ex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdBI() {
        return idBI;
    }

    public void setIdBI(int idBI) {
        this.idBI = idBI;
    }

    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public int getSel() {
        return sel;
    }

    public void setSel(int sel) {
        this.sel = sel;
    }

    public String getOderDate() {
        return oderDate;
    }

    public void setOderDate(String oderDate) {
        this.oderDate = oderDate;
    }

    public String getReceieveDate() {
        return receieveDate;
    }

    public void setReceieveDate(String receieveDate) {
        this.receieveDate = receieveDate;
    }

    public int getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(int totalUnit) {
        this.totalUnit = totalUnit;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    @Override
    public String toString() {
        return "BuyInGredients{" + "id=" + id + ", idBI=" + idBI + ", idEmployee=" + idEmployee + ", sel=" + sel + ", oderDate=" + oderDate + ", receieveDate=" + receieveDate + ", totalUnit=" + totalUnit + ", totalPrice=" + totalPrice + ", netAmount=" + netAmount + '}';
    }

    public static BuyInGredients fromRS(ResultSet rs) {
        BuyInGredients buyInGredients = new BuyInGredients();
        try {
            buyInGredients.setId(rs.getInt("ID_DBI"));
            buyInGredients.setIdEmployee(rs.getInt("ID_EM"));
            buyInGredients.setSel(rs.getInt("ID_SEL"));

            buyInGredients.setOderDate(rs.getString("BI_ORDER_DATE"));

            buyInGredients.setReceieveDate(rs.getString("BI_RECEIEVE_DATE"));
            buyInGredients.setIdBI(rs.getInt("ID_BI"));

            buyInGredients.setTotalUnit(rs.getInt("BI_TOTAL_UNIT"));
            buyInGredients.setTotalPrice(rs.getDouble("BI_TOTAL_PRICE"));
            buyInGredients.setNetAmount(rs.getDouble("BI_TOTAL_NET_AMOUNT"));
        } catch (SQLException ex) {
            Logger.getLogger(BuyInGredients.class.getName()).log(Level.SEVERE, null, ex);
            // จัดการข้อผิดพลาดที่เกิดขึ้นในที่นี้
        }
        return buyInGredients;
    }

}
