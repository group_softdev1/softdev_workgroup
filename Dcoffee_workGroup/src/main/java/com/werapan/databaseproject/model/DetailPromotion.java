/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class DetailPromotion {

    private int id;
    private int idMenu;
    private int idProm;
    private String DepromDetail;
    private int discount;

    public DetailPromotion(int id, int idMenu, int idProm, String DepromDetail, int discount) {
        this.id = id;
        this.idMenu = idMenu;
        this.idProm = idProm;
        this.DepromDetail = DepromDetail;
        this.discount = discount;
    }

    public DetailPromotion(int idMenu, int idProm, String DepromDetail, int discount) {
        this.id = -1;
        this.idMenu = idMenu;
        this.idProm = idProm;
        this.DepromDetail = DepromDetail;
        this.discount = discount;
    }

    public DetailPromotion() {
        this.id = -1;
        this.idMenu = 0;
        this.idProm = 0;
        this.DepromDetail = "";
        this.discount = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public int getIdProm() {
        return idProm;
    }

    public void setIdProm(int idProm) {
        this.idProm = idProm;
    }

    public String getDepromDetail() {
        return DepromDetail;
    }

    public void setDepromDetail(String DepromDetail) {
        this.DepromDetail = DepromDetail;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "DetailPromotion{" + "id=" + id + ", idMenu=" + idMenu + ", idProm=" + idProm + ", DepromDetail=" + DepromDetail + ", discount=" + discount + '}';
    }

    public static DetailPromotion fromRS(ResultSet rs) {
        DetailPromotion detailPromotion = new DetailPromotion();
        try {
            detailPromotion.setId(rs.getInt("ID_DEPROM"));
            detailPromotion.setIdMenu(rs.getInt("ID_MENU"));
            detailPromotion.setIdProm(rs.getInt("ID_PROM"));
            detailPromotion.setDepromDetail(rs.getString("DEPROM_DETAIL"));
            detailPromotion.setDiscount(rs.getInt("DEPROM_DISCOUNT"));

        } catch (SQLException ex) {
            Logger.getLogger(DetailPromotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return detailPromotion;
    }


}
