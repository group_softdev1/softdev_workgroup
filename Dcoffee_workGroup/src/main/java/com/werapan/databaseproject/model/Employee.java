/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class Employee {

    public static Employee currentUser;

    private int id;

    private String fullName;

    private Double moneyHr;
    private String tel;
    private String login;
    private String password;
    private String role;
    private String gender;

    public Employee(int id, String fullName, Double moneyHr, String tel, String login, String password, String role, String gender) {
        this.id = id;
        this.fullName = fullName;
        this.moneyHr = moneyHr;
        this.tel = tel;
        this.login = login;
        this.password = password;
        this.role = role;
        this.gender = gender;
    }

    public Employee(String fullName, Double moneyHr, String tel, String login, String password, String role, String gender) {
        this.id = -1;
        this.fullName = fullName;
        this.moneyHr = moneyHr;
        this.tel = tel;
        this.login = login;
        this.password = password;
        this.role = role;
        this.gender = gender;
    }

    public Employee() {
        this.id = -1;
        this.fullName = "";
        this.moneyHr = 0.0;
        this.tel = "";
        this.login = "";
        this.password = "";
        this.role = "";
        this.gender = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Double getMoneyHr() {
        return moneyHr;
    }

    public void setMoneyHr(Double moneyHr) {
        this.moneyHr = moneyHr;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("ID_EM"));
           
            employee.setLogin(rs.getString("EM_LOGIN"));
           
            employee.setFullName(rs.getString("EM_FULLNAME"));
            employee.setPassword(rs.getString("EM_PASSWORD"));
           
            employee.setRole(rs.getString("EM_ROLE"));
            employee.setGender(rs.getString("EM_GENDER"));
            employee.setMoneyHr(rs.getDouble("EM_MONEY_PER_UNIT"));
            employee.setTel(rs.getString("EM_TEL"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

    public static Employee getCurrentUser() {
        return new Employee(1, "JO", 250.0, "0944501992", "JO", "1234", "A", "M");
    }

}
