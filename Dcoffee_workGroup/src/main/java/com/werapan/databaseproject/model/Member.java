/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class Member {

    private int id;
    private String name;
    
    private String dateRegister;
    private int point;
    private String tel;

    public Member(int id, String name, String dateRegister, int point, String tel) {
        this.id = id;
        this.name = name;
        
        this.dateRegister = dateRegister;
        this.point = point;
        this.tel = tel;
    }

    public Member(String name, String dateRegister, int point, String tel) {
        this.id = -1;
        this.name = name;
        
        this.dateRegister = dateRegister;
        this.point = point;
        this.tel = tel;
    }

    public Member() {
        this.id = -1;
         
        this.name = "";
        
        this.dateRegister = "";
        this.point = 0;
        this.tel = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", dateRegister=" + dateRegister + ", point=" + point + ", tel=" + tel + '}';
    }

    public static Member fromRS(ResultSet rs){
        Member member = new Member();
        try {
            member.setId(rs.getInt("ID_MEMBER"));
            member.setName(rs.getString("MEMBER_FNAME_LNAME"));
         
            member.setDateRegister(rs.getString("MEMBER_DATE_REGISTER"));
            member.setPoint(rs.getInt("MEMBER_POINT_MEMBER"));
            member.setTel(rs.getString("MEMBER_TEL"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }

    
}
