/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author informatics
 */
public class DetailBuyIngredientModel {
    
   private int id;
   private int idIGD;
  
   private String name;
   private int qtt;
   private int price;

    public DetailBuyIngredientModel(int id, int idIGD, String name, int qtt, int price) {
        this.id = id;
        this.idIGD = idIGD;
        
        this.name = name;
        this.qtt = qtt;
        this.price = price;
    }

    public DetailBuyIngredientModel(int idIGD, String name, int qtt, int price) {
        this.id = -1;
        this.idIGD = idIGD;
        
        this.name = name;
        this.qtt = qtt;
        this.price = price;
    }

    public DetailBuyIngredientModel() {
        this.id = -1;
        this.idIGD = 0;
       
        this.name = "";
        this.qtt = 0;
        this.price = 0;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdIGD() {
        return idIGD;
    }

    public void setIdIGD(int idIGD) {
        this.idIGD = idIGD;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQtt() {
        return qtt;
    }

    public void setQtt(int qtt) {
        this.qtt = qtt;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "DetailBuyIngredientModel{" + "id=" + id + ", idIGD=" + idIGD + ", name=" + name + ", qtt=" + qtt + ", price=" + price + '}';
    }

   

   
    public static DetailBuyIngredientModel fromRS(ResultSet rs) {
        DetailBuyIngredientModel detailBuyIngredientModel = new DetailBuyIngredientModel();
        try {
            detailBuyIngredientModel.setId(rs.getInt("ID_BI"));
            detailBuyIngredientModel.setIdIGD(rs.getInt("ID_IGD"));
            
            detailBuyIngredientModel.setName(rs.getString("DBI_NAME"));
            detailBuyIngredientModel.setQtt(rs.getInt("DBI_QUANTITY_P_BUY"));
            detailBuyIngredientModel.setPrice(rs.getInt("DBI_PRICE_P_BUY"));

        } catch (SQLException ex) {
            Logger.getLogger(DetailBuyIngredientModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return detailBuyIngredientModel;
    }
}
