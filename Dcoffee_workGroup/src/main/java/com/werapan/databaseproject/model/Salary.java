/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Salary {
    private int id;
    private int idEM;
    private int total;
    private int workingHours;
    private String status;
            
            
    public Salary(int idSPD, int idEM, int spdTotal, int workingHours, String status) {
        this.id = idSPD;
        this.idEM = idEM;
        this.total = spdTotal;
        this.workingHours = workingHours;
        this.status = status ;
    }

    public Salary(int idEM, int spdTotal, int workingHours, String status) {
        this.id = -1 ;
        this.idEM = idEM;
        this.total = spdTotal;
        this.workingHours = workingHours;
        this.status = status ;
    }

    public Salary() {
        this.id = -1 ;
        this.idEM = 0;
        this.total = 0;
        this.workingHours = 0;
        this.status = "" ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEM() {
        return idEM;
    }

    public void setIdEM(int idEM) {
        this.idEM = idEM;
    }


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    public int getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(int workingHours) {
        this.workingHours = workingHours;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", idEM=" + idEM + ", total=" + total +", workingHours=" + workingHours + ", status=" + status +'}';
    }
    
    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("ID_SPD"));
            salary.setIdEM(rs.getInt("ID_EM"));
            salary.setTotal(rs.getInt("SPD_TOTAL"));
            salary.setWorkingHours(rs.getInt("SPD_TOTAL_WORKING_HOURS"));
            salary.setStatus(rs.getString("SPD_STATUS"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
    
}
