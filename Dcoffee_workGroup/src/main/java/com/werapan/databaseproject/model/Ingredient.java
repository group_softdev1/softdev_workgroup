/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Ingredient {
    private int id;
    private String name;
    private int amount;
    private double price;

    public Ingredient(int id, String name, int amount, double price) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    
    
    public Ingredient(String name, int amount, double price) {
        this.id = -1;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
    
    public Ingredient() {
        this.id = -1;
        this.name = "";
        this.amount = 0;
        this.price = 0.0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "id=" + id + ", name=" + name + ", amount=" + amount + ", price=" + price + '}';
    }
    
    public static Ingredient fromRS(ResultSet rs){
        Ingredient ingredient = new Ingredient();
        try {
            ingredient.setId(rs.getInt("ID_IGD"));
            ingredient.setName(rs.getString("IGD_NAME"));
            ingredient.setAmount(rs.getInt("IGD_AMOUNT"));
            ingredient.setPrice(rs.getDouble("IGD_PRICE"));   
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredient;
    }
}
