/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siwak
 */


public class ReceiptDetail {
    private int id;
    private int idProduct;
    private int idReceipt;
    private int idDeprom;
    private int totalMenu;
    private int menuPrice;
//    private double discount;
    private int netamount;
    
    private int bestSales;
    private int totalQty;
    private double total;

    public ReceiptDetail(int id, int idProduct, int idReceipt, int idDeprom, int totalMenu, int menuPrice, int netamount) {
        this.id = id;
        this.idProduct = idProduct;
        this.idReceipt = idReceipt;
        this.idDeprom = idDeprom;
        this.totalMenu = totalMenu;
        this.menuPrice = menuPrice;
//        this.discount = discount;
        this.netamount = netamount;
    }
    
    public ReceiptDetail(int idProduct, int idReceipt, int idDeprom, int totalMenu, int menuPrice, int netamount) {
        this.id = -1;
        this.idProduct = idProduct;
        this.idReceipt = idReceipt;
        this.idDeprom = idDeprom;
        this.totalMenu = totalMenu;
        this.menuPrice = menuPrice;
//        this.discount = discount;
        this.netamount = netamount;
    }
    
    public ReceiptDetail(int idProduct, int totalMenu, int menuPrice, int netamount) {
        this.id = -1;
        this.idProduct = idProduct;
        this.totalMenu = totalMenu;
        this.menuPrice = menuPrice;
        this.netamount = netamount;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.idProduct = 0;
        this.idReceipt = 0;
        this.idDeprom = 0;
        this.totalMenu = 0;
        this.menuPrice = 0;
//        this.discount = 0;
        this.netamount = 0;
    }

    public int getId() {    
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idMenu) {
        this.idProduct = idMenu;
    }

    public int getIdReceipt() {
        return idReceipt;
    }

    public void setIdReceipt(int idReceipt) {
        this.idReceipt = idReceipt;
    }

    public int getIdDeprom() {
        return idDeprom;
    }

    public void setIdDeprom(int idDeprom) {
        this.idDeprom = idDeprom;
    }


    public int getTotalMenu() {
        return totalMenu;
    }

    public void setTotalMenu(int qty) {
        this.totalMenu = qty;
        netamount = menuPrice * qty;
    }

    public int getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(int menuPrice) {
        this.menuPrice = menuPrice;
    }

//    public double getDiscount() {
//        return discount;
//    }
//
//    public void setDiscount(double discount) {
//        this.discount = discount;
//    }

    public int getNetamount() {
        return netamount;
    }

    public void setNetamount(int netamount) {
        this.netamount = netamount;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", idMenu=" + idProduct + ", idReceipt=" + idReceipt + ", idDeprom=" + idDeprom + ", totalMenu=" + totalMenu + ", menuPrice=" + menuPrice + ", netamount=" + netamount + '}';
    }

//        public void calculateTotal(){
//        int totalQuantity = 0;
//        double netAmount = 0.0;
//        ArrayList<ReceiptDetail> List = new ArrayList<ReceiptDetail>();
//        for(ReceiptDetail rd: List){
//            netAmount += rd.getNetamount();
//            totalQuantity += rd.getTotalMenu();
//        }
//        this.totalQty = totalQuantity;
//        this.netAmount \= netAmount;
//    }
        
    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail recieptDetial = new ReceiptDetail();
        try {
            recieptDetial.setId(rs.getInt("ID_RECDETAIL"));
            recieptDetial.setIdProduct(rs.getInt("ID_PRODUCT"));
            recieptDetial.setIdReceipt(rs.getInt("ID_RECEIPT"));
            recieptDetial.setIdDeprom(rs.getInt("ID_DEPROM"));
            recieptDetial.setTotalMenu(rs.getInt("RECDETAIL_TOTAL_MENU"));
            recieptDetial.setMenuPrice(rs.getInt("RECDETAIL_MENU_PRICE"));
//            recieptDetial.setDiscount(rs.getDouble("RECDETAIL_DISCOUNT"));
            recieptDetial.setNetamount(rs.getInt("RECEIPT_NET_AMOUNT"));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetial;
    }
}
