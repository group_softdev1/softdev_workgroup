/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Bill {

    private int id;
    private int idEM;
    private String list;
    private String startDate;
    private String endDate;
    private double thisNumber;
    private double pernumber;
    private double unit;
    private double perUnit;
    private double price;
    private double totalPrice;

    public Bill(int id, int idEM, String list, String startDate, String endDate, double thisNumber, double pernumber, double unit, double perUnit, double price, double totalPrice) {
        this.id = id;
        this.idEM = idEM;
        this.list = list;
        this.startDate = startDate;
        this.endDate = endDate;
        this.thisNumber = thisNumber;
        this.pernumber = pernumber;
        this.unit = unit;
        this.perUnit = perUnit;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public Bill(int idEM, String list, String startDate, String endDate, double thisNumber, double perNumber, double unit, double perUnit, double price, double totalPrice) {
        this.id = -1;
        this.idEM = idEM;
        this.list = list;
        this.startDate = startDate;
        this.endDate = endDate;
        this.thisNumber = thisNumber;
        this.pernumber = perNumber;
        this.unit = unit;
        this.perUnit = perUnit;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public Bill() {
        this.id = -1;
        this.idEM = 0;
        this.list = "";
        this.startDate = "";
        this.endDate = "";
        this.thisNumber = 0.0;
        this.pernumber = 0.0;
        this.unit = 0.0;
        this.perUnit = 0.0;
        this.price = 0.0;
        this.totalPrice = 0.0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEM() {
        return idEM;
    }

    public void setIdEM(int idEM) {
        this.idEM = idEM;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public double getThisNumber() {
        return thisNumber;
    }

    public void setThisNumber(double thisNumber) {
        this.thisNumber = thisNumber;
    }

    public double getPernumber() {
        return pernumber;
    }

    public void setPernumber(double pernumber) {
        this.pernumber = pernumber;
    }

    public double getUnit() {
        return unit;
    }

    public void setUnit(double unit) {
        this.unit = unit;
    }

    public double getPerUnit() {
        return perUnit;
    }

    public void setPerUnit(double perUnit) {
        this.perUnit = perUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", idEM=" + idEM + ", list=" + list + ", startDate=" + startDate + ", endDate=" + endDate + ", thisNumber=" + thisNumber + ", pernumber=" + pernumber + ", unit=" + unit + ", perUnit=" + perUnit + ", price=" + price + ", totalPrice=" + totalPrice + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("ID_BILL"));
            bill.setIdEM(rs.getInt("ID_EM"));
            bill.setList(rs.getString("LIST"));
            bill.setStartDate(rs.getString("START_DATE"));
            bill.setEndDate(rs.getString("END_DATE"));
            bill.setThisNumber(rs.getDouble("THISNUMBER"));
            bill.setPernumber(rs.getDouble("PERNUMBER"));
            bill.setUnit(rs.getDouble("UNIT"));
            bill.setPerUnit(rs.getDouble("PERUNIT"));
            bill.setPrice(rs.getDouble("PRICE"));
            bill.setTotalPrice(rs.getDouble("TOTALPRICE"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }

}
