/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

//import com.werapan.databaseproject.dao.BranchDao;
//import com.werapan.databaseproject.dao.EmployeeDao;
//import com.werapan.databaseproject.dao.MemberDao;
import com.werapan.databaseproject.helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siwak
 */
public class Receipt {

    ArrayList<ReceiptDetail> recieptDetails = new ArrayList<ReceiptDetail>();
    private int IDReceipt = -1;
    private int id;
    private int idEm;
    private int idMember;
    private Date date;
    private int totalQuantity;
    private float totalPrice;
    private float discount;
    private double netAmount;
    private Employee employee;
    private Member member;
    private int totalQty;
    private double total;
    private double balance;
    private double netAmountPos;
    private double discountPos;
    private double moneyRecived;

    public double getMoneyRecived() {
        return moneyRecived;
    }

    public void setMoneyRecived(double moneyRecived) {
        this.moneyRecived = moneyRecived;
    }

    public double getDiscountPos() {
        return discountPos;
    }

    public void setDiscountPos(double discountPos) {
        this.discountPos = discountPos;
    }

    public double getNetAmountPos() {
        return netAmountPos;
    }

    public void setNetAmountPos(double netAmountPos) {
        this.netAmountPos = netAmountPos;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Receipt(int idEm, int idMember, int totalQuantity, float totalPrice, float discount, double netAmount) {

        this.idEm = idEm;
        this.idMember = idMember;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netAmount = netAmount;

    }

    public Receipt(int id, int idEm, int idMember, Date date, int totalQuantity, float totalPrice, float discount, double netAmount) {
        this.id = id;
        this.idEm = idEm;
        this.idMember = idMember;
        this.date = date;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netAmount = netAmount;

    }

    public Receipt(int idEm, int idMember, Date date, int totalQuantity, float totalPrice, float discount, double netAmount, int totalQty, double total) {

        this.idEm = idEm;
        this.idMember = idMember;
        this.date = date;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netAmount = netAmount;
        this.totalQty = totalQty;
        this.total = total;
    }

    public Receipt() {
        this.IDReceipt = getCurrentID();
        this.id = this.IDReceipt;
        this.idEm = -1;
        this.idMember = -1;
        this.date = null;
        this.totalQuantity = 0;
        this.totalPrice = (float) 0.00;
        this.discount = (float) 0.00;
        this.netAmount = 0;
    }

    public ArrayList<ReceiptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList<ReceiptDetail> recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEm() {
        return idEm;
    }

    public void setIdEm(int idEm) {
        this.idEm = idEm;
    }

    public int getIdMember() {
        return idMember;
    }

    public void setIdMember(int idMember) {
        this.idMember = idMember;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.idEm = employee.getId();
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
        this.idMember = member.getId();
    }

    @Override
    public String toString() {
        return "Receipt{" + "recieptDetails=" + recieptDetails + ", id=" + id + ", idEm=" + idEm + ", idMember=" + idMember + ", date=" + date + ", totalQuantity=" + totalQuantity + ", totalPrice=" + totalPrice + ", discount=" + discount + ", netAmount=" + netAmount + ", totalQty=" + totalQty + ", total=" + total + '}';
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("ID_RECEIPT"));
            receipt.setIdEm(rs.getInt("ID_EM"));
            receipt.setIdMember(rs.getInt("ID_MEMBER"));
            receipt.setDate(rs.getTimestamp("RECEIPT_DATE"));
            receipt.setTotalQuantity(rs.getInt("RECEIPT_TOTAL_QUANTITY"));
            receipt.setTotalPrice(rs.getFloat("RECEIPT_TOTAL_PRICE"));
            receipt.setDiscount(rs.getFloat("RECEIPT_DISCOUNT"));
            receipt.setNetAmount(rs.getInt("RECEIPT_NET_AMOUNT"));

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }

    public void addReceiptDetails(ReceiptDetail receiptDetail) {
        recieptDetails.add(receiptDetail);
        calculateTotal();
    }

    public void delReceiptDetails(ReceiptDetail receiptDetail) {
        recieptDetails.remove(receiptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQuantity = 0;
        double netAmount = 0.0;

        for (ReceiptDetail rd : recieptDetails) {
            netAmount += rd.getNetamount();
            totalQuantity += rd.getTotalMenu();
        }
        this.totalQuantity = totalQuantity;
        this.netAmount = netAmount;
    }

    public void addRecieptDetail(Product product, int qty) {
        ReceiptDetail rd = new ReceiptDetail(product.getId(), qty, product.getPrice(), qty * product.getPrice());
        recieptDetails.add(rd);
        calculateTotal();
    }

    private int getCurrentID() {
        int currentID = -1;
        String sql = "SELECT MAX(ID_RECEIPT) FROM RECEIPT";
        //stmt = conn.createStatement();

        Connection conn = DatabaseHelper.getConnect();
        try {

            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                currentID = rs.getInt(1);
            }
            currentID++;
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);

            
        }
        return currentID;
    }
}
