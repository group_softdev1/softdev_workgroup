/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Branch {
    private int id;
    private String name;
    private String phone;
    private String location;

    public Branch(int id, String name, String phone, String location) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.location = location;
    }

    public Branch(String name, String phone, String location) {
        this.id = -1;
        this.name = name;
        this.phone = phone;
        this.location = location;
    }

    public Branch() {
        this.id = -1;
        this.name = "";
        this.phone = "";
        this.location = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Branch{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", location=" + location + '}';
    }

   
  
    public static Branch fromRS(ResultSet rs) {
        Branch branch = new Branch();
        try {
            branch.setId(rs.getInt("ID_BRANCH"));
            branch.setName(rs.getString("BRANCH_NAME"));
            branch.setPhone(rs.getString("BRANCH_PHONE"));
            branch.setLocation(rs.getString("BRANCH_LOCATION"));
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return branch;
    }

    
}
