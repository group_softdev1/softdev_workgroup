/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Murphy
 */
public class Seller {

    private int id;
    private String nameCompany;
    private String tradingCompany;

    public Seller(int id, String nameCompany, String tradingCompany) {
        this.id = id;
        this.nameCompany = nameCompany;
        this.tradingCompany = tradingCompany;
    }

    public Seller(String nameCompany, String tradingCompany) {
        this.id = -1;
        this.nameCompany = nameCompany;
        this.tradingCompany = tradingCompany;
    }

    public Seller() {
        this.id = -1;
        this.nameCompany = "";
        this.tradingCompany = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getTradingCompany() {
        return tradingCompany;
    }

    public void setTradingCompany(String tradingCompany) {
        this.tradingCompany = tradingCompany;
    }

    public static Seller fromRS(ResultSet rs) {
        Seller seller = new Seller();
        try {
            seller.setId(rs.getInt("ID_SEL"));
            seller.setNameCompany(rs.getString("SEL_NAME_COMPANY"));
            seller.setTradingCompany(rs.getString("SEL_TRADING_COMPANY"));

        } catch (SQLException ex) {
            Logger.getLogger(Seller.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return seller;
    }
}
