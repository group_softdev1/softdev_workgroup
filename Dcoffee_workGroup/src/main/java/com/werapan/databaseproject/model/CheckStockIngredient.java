/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class CheckStockIngredient {
    private int id;
    private int idEM;
    private String date;

    public CheckStockIngredient(int id, int name, String date) {
        this.id = id;
        this.idEM = name;
        this.date = date;
    }

    public CheckStockIngredient(int name, String date) {
        this.id = -1;
        this.idEM = name;
        this.date = date;
    }

    public CheckStockIngredient() {
        this.id = -1;
        this.idEM = 0;
        this.date = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEM() {
        return idEM;
    }

    public void setIdEM(int idEM) {
        this.idEM = idEM;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CheckStockIngredient{" + "id=" + id + ", idEM=" + idEM + ", date=" + date + '}';
    }

    public static CheckStockIngredient fromRS(ResultSet rs) {
        CheckStockIngredient checkStockIngredient = new CheckStockIngredient();
        try {
            checkStockIngredient.setId(rs.getInt("ID_CS"));
            checkStockIngredient.setIdEM(rs.getInt("ID_EM"));
            checkStockIngredient.setDate(rs.getString("CS_DATE"));
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockIngredient;
    }
    
}


