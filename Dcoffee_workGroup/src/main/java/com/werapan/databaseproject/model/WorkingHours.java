/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class WorkingHours {
    private int id;
    private int idEM;
    private String WTimeIn;
    private String WTimeOut;
    private int total;

    public WorkingHours(int id, int idEM, String WTimeIn, String WTimeOut, int total) {
        this.id = id;
        this.idEM = idEM;
        this.WTimeIn = WTimeIn;
        this.WTimeOut = WTimeOut;
        this.total = total;
    }

    public WorkingHours(int idEM, String WTimeIn, String WTimeOut, int total) {
        this.id = -1 ;
        this.idEM = idEM;
        this.WTimeIn = WTimeIn;
        this.WTimeOut = WTimeOut;
        this.total = total;
    }

    public WorkingHours() {
        this.id = -1 ;
        this.idEM = 0;
        this.WTimeIn = "";
        this.WTimeOut = "";
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEM() {
        return idEM;
    }

    public void setIdEM(int idEM) {
        this.idEM = idEM;
    }

    public String getWTimeIn() {
        return WTimeIn;
    }

    public void setWTimeIn(String WTimeIn) {
        this.WTimeIn = WTimeIn;
    }

    public String getWTimeOut() {
        return WTimeOut;
    }

    public void setWTimeOut(String WTimeOut) {
        this.WTimeOut = WTimeOut;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "WorkingHours{" + "id=" + id + ", idEM=" + idEM + ", WTimeIn=" + WTimeIn + ", WTimeOut=" + WTimeOut + ", total=" + total + '}';
    }
    
    public static WorkingHours fromRS(ResultSet rs) {
        WorkingHours workingHours = new WorkingHours();
        try {
            workingHours.setId(rs.getInt("ID_WORKINGHOURS"));
            workingHours.setIdEM(rs.getInt("ID_EM"));
            workingHours.setWTimeIn(rs.getString("WORKH_TIME_IN"));
            workingHours.setWTimeOut(rs.getString("WORKH_TIME_OUT"));
            workingHours.setTotal(rs.getInt("WORKH_TOTAL"));
        } catch (SQLException ex) {
            Logger.getLogger(WorkingHours.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workingHours;
    }
}
