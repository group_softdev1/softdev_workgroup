/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM SALARYPAYMENTDETAILS WHERE ID_SPD= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                salary = new Salary();
                salary.setId(rs.getInt("ID_SPD"));
                salary.setIdEM(rs.getInt("ID_EM"));
                salary.setTotal(rs.getInt("SPD_TOTAL"));
                salary.setWorkingHours(rs.getInt("SPD_TOTAL_WORKING_HOURS"));
                salary.setStatus(rs.getString("SPD_STATUS"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARYPAYMENTDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = new Salary();
                salary.setId(rs.getInt("ID_SPD"));
                salary.setIdEM(rs.getInt("ID_EM"));
                salary.setTotal(rs.getInt("SPD_TOTAL"));
                salary.setWorkingHours(rs.getInt("SPD_TOTAL_WORKING_HOURS"));
                salary.setStatus(rs.getString("SPD_STATUS"));

                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Salary save(Salary obj) {
        String sql = "INSERT INTO SALARYPAYMENTDETAILS(ID_EM, SPD_TOTAL, SPD_TOTAL_WORKING_HOURS, SPD_STATUS)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setInt(2, obj.getTotal());
            stmt.setInt(3, obj.getWorkingHours());
            stmt.setString(4, obj.getStatus());
//            System.out.println(stmt); 
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Salary update(Salary obj) {
        String sql = "UPDATE SALARYPAYMENTDETAILS"
                + " SET ID_EM = ? , SPD_TOTAL = ?, SPD_TOTAL_WORKING_HOURS=?, SPD_STATUS=?"
                + " WHERE  ID_SPD = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setInt(2, obj.getTotal());
            stmt.setInt(3, obj.getWorkingHours());
            stmt.setString(4, obj.getStatus());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(Salary obj) {
        String sql = "DELETE FROM SALARYPAYMENTDETAILS WHERE ID_SPD=" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARYPAYMENTDETAILS where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARYPAYMENTDETAILS  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
