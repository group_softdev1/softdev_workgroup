/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Seller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Murphy
 */
public class SellerDao implements Dao<Seller>{
    
        @Override
    public Seller get(int id) {
        Seller seller = null;
        String sql = "SELECT * FROM SELLER WHERE ID_SEL =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                seller = Seller.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return seller;
    }

    @Override
    public List<Seller> getAll() {
        ArrayList<Seller> list = new ArrayList();
        String sql = "SELECT * FROM SELLER";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Seller seller = Seller.fromRS(rs);
                list.add(seller);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Seller> getAll(String where, String order) {
        ArrayList<Seller> list = new ArrayList();
        String sql = "SELECT * FROM SELLER where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Seller seller = Seller.fromRS(rs);
                list.add(seller);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<Seller> getAll(String order) {
        ArrayList<Seller> list = new ArrayList();
        String sql = "SELECT * FROM SELLER ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Seller seller = Seller.fromRS(rs);
                list.add(seller);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Seller save(Seller obj) {
        String sql = "INSERT INTO SELLER(ID_SEL, SEL_NAME_COMPANY, SEL_TRADING_COMPANY)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getNameCompany());
            stmt.setString(3, obj.getTradingCompany());
            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Seller update(Seller obj) {
        String sql = "UPDATE SELLER"
                + " SET SEL_NAME_COMPANY = ?, SEL_TRADING_COMPANY = ?"
                + " WHERE ID_SEL = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getNameCompany());
            stmt.setString(3, obj.getTradingCompany());
            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public int delete(Seller obj) {
        String sql = "DELETE FROM  SELLER WHERE ID_SEL=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

}
