/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.CheckStockIngredient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class CheckStockIngredientDao implements Dao<CheckStockIngredient> {

    @Override
    public CheckStockIngredient get(int id) {
        CheckStockIngredient checkStockIngredient = null;
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENT WHERE ID_CS= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                checkStockIngredient = new CheckStockIngredient();
                checkStockIngredient.setId(rs.getInt("ID_CS"));
                checkStockIngredient.setIdEM(rs.getInt("ID_EM"));
                checkStockIngredient.setDate(rs.getString("CS_DATE"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockIngredient;
    }

    @Override
    public List<CheckStockIngredient> getAll() {
        ArrayList<CheckStockIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredient checkStockIngredient = new CheckStockIngredient();
                checkStockIngredient.setId(rs.getInt("ID_CS"));
                checkStockIngredient.setIdEM(rs.getInt("ID_EM"));
                checkStockIngredient.setDate(rs.getString("CS_DATE"));

                list.add(checkStockIngredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStockIngredient save(CheckStockIngredient obj) {
        String sql = "INSERT INTO CHECKSTOCKINGREDIENT(ID_EM, CS_DATE)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getDate());

            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public CheckStockIngredient update(CheckStockIngredient obj) {
        String sql = "UPDATE CHECKSTOCKINGREDIENT"
                + " SET ID_EM = ? , CS_DATE = ?"
                + " WHERE  ID_CS = ?";
        Connection conn = DatabaseHelper.getConnect();  
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getDate());
            stmt.setInt(3, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(CheckStockIngredient obj) {
        String sql = "DELETE FROM CHECKSTOCKINGREDIENT WHERE ID_CS=" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckStockIngredient> getAll(String where, String order) {
        ArrayList<CheckStockIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredient checkStockIngredient = CheckStockIngredient.fromRS(rs);
                list.add(checkStockIngredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStockIngredient> getAll(String order) {
        ArrayList<CheckStockIngredient> list = new ArrayList();
        String sql = "SELECT * FROM CHECKSTOCKINGREDIENT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockIngredient checkStockIngredient = CheckStockIngredient.fromRS(rs);
                list.add(checkStockIngredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
