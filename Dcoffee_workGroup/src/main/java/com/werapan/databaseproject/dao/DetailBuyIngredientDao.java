/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.DetailBuyIngredientModel;
import com.werapan.databaseproject.model.DetailBuyIngredientModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class DetailBuyIngredientDao implements Dao<DetailBuyIngredientModel> {

    @Override
    public DetailBuyIngredientModel get(int id) {
        DetailBuyIngredientModel detailBuyIngredientModel = null;
        String sql = "SELECT * FROM DATAIL_BUY_INGREDIENT WHERE ID_BI =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                detailBuyIngredientModel = detailBuyIngredientModel.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return detailBuyIngredientModel;

    }

    @Override
    public List<DetailBuyIngredientModel> getAll() {
        ArrayList<DetailBuyIngredientModel> list = new ArrayList();
        String sql = "SELECT * FROM DATAIL_BUY_INGREDIENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailBuyIngredientModel detailBuyIngredientModel = DetailBuyIngredientModel.fromRS(rs);
                list.add(detailBuyIngredientModel);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public List<DetailBuyIngredientModel> getAll(String where, String order) {
        ArrayList<DetailBuyIngredientModel> list = new ArrayList();
        String sql = "SELECT * FROM DATAIL_BUY_INGREDIENT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailBuyIngredientModel detailBuyIngredientModel = DetailBuyIngredientModel.fromRS(rs);
                list.add(detailBuyIngredientModel);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<DetailBuyIngredientModel> getAll(String order) {
        ArrayList<DetailBuyIngredientModel> list = new ArrayList();
        String sql = "SELECT * FROM DATAIL_BUY_INGREDIENT ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailBuyIngredientModel detailBuyIngredientModel = DetailBuyIngredientModel.fromRS(rs);
                list.add(detailBuyIngredientModel);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public DetailBuyIngredientModel save(DetailBuyIngredientModel obj) {

        String sql = "INSERT INTO DATAIL_BUY_INGREDIENT(ID_IGD, DBI_NAME, DBI_QUANTITY_P_BUY, DBI_PRICE_P_BUY)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, obj.getIdIGD());
          
            stmt.setString(2, obj.getName());
            stmt.setInt(3, obj.getQtt());
            stmt.setInt(4, obj.getPrice());

            int ret = stmt.executeUpdate();

            
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public DetailBuyIngredientModel update(DetailBuyIngredientModel obj) {
        String sql = "UPDATE DATAIL_BUY_INGREDIENT"
                + " SET ID_IGD =  ? , DBI_NAME =  ?,DBI_QUANTITY_P_BUY =  ?,DBI_PRICE_P_BUY =  ?"
                + " WHERE  ID_BI = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, obj.getIdIGD());
           
            stmt.setString(2, obj.getName());
            stmt.setInt(3, obj.getQtt());
            stmt.setInt(4, obj.getPrice());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(DetailBuyIngredientModel obj) {
        String sql = "DELETE FROM DATAIL_BUY_INGREDIENT WHERE ID_BI =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
