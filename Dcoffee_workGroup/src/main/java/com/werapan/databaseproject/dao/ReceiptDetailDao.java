package com.werapan.databaseproject.dao;
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail receiptDetail = null;
        String sql = "SELECT * FROM RECEIPTDETAILS WHERE ID_RECDETAIL=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptDetail = ReceiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptDetail;
    }
    
    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<ReceiptDetail> getAll(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAILS where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPTDETAILS  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getBestSales(int limit) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT PRODUCT_NAME, MAX(RECDETAIL_TOTAL_MENU) AS MAX_QTY" // RECDETAIL_TOTAL_MENU,
                    + "FROM RECEIPTDETAILS NATURAL JOIN PRODUCT"
                    + "GROUP BY PRODUCT_NAME"
                    + "ORDER BY MAX_QTY DESC"
                    + "LIMIT "+ limit;
                                                
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                receiptDetail.setIdProduct(rs.getInt("PROUCT_NAME"));
//                receiptDetail.setTotalMenu(rs.getInt("RECDETAIL_TOTAL_MENU"));
                list.add(receiptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public ReceiptDetail save(ReceiptDetail obj) {

        String sql = "INSERT INTO RECEIPTDETAILS (ID_PRODUCT, ID_RECEIPT, ID_DEPROM, RECDETAIL_TOTAL_MENU, RECDETAIL_MENU_PRICE, RECEIPT_NET_AMOUNT)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdProduct());
            stmt.setInt(2, obj.getIdReceipt());
            stmt.setInt(3, obj.getIdDeprom());
            stmt.setInt(4, obj.getTotalMenu());
            stmt.setInt(5, obj.getMenuPrice());
//            stmt.setDouble(6, obj.getDiscount());
            stmt.setInt(6, obj.getNetamount());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE RECEIPTDETAILS"
                + " SET ID_PRODUCT = ?, ID_RECEIPT = ?, ID_DEPROM = ?, RECDETAIL_TOTAL_MENU = ?, RECDETAIL_MENU_PRICE = ?, RECEIPT_NET_AMOUNT = ?"
                + " WHERE ID_RECDETAIL = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdProduct());
            stmt.setInt(2, obj.getIdReceipt());
            stmt.setInt(3, obj.getIdDeprom());
            stmt.setInt(4, obj.getTotalMenu());
            stmt.setInt(5, obj.getMenuPrice());
//            stmt.setDouble(6, obj.getDiscount());
            stmt.setInt(6, obj.getNetamount());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM RECEIPTDETAILS WHERE ID_RECDETAIL=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
