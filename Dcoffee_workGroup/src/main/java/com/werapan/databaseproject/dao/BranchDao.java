/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Branch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BranchDao implements Dao<Branch> {

    public static Branch getByName() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Branch getByName(String name) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE BRANCH_NAME= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                branch = new Branch();
                branch.setId(rs.getInt("ID_BRANCH"));
                branch.setName(rs.getString("BRANCH_NAME"));
                branch.setPhone(rs.getString("BRANCH_PHONE"));
                branch.setLocation(rs.getString("BRANCH_LOCATION"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }

    @Override
    public Branch get(int id) {
        Branch branch = null;
        String sql = "SELECT * FROM BRANCH WHERE ID_BRANCH= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                branch = new Branch();
                branch.setId(rs.getInt("ID_BRANCH"));
                branch.setName(rs.getString("BRANCH_NAME"));
                branch.setPhone(rs.getString("BRANCH_PHONE"));
                branch.setLocation(rs.getString("BRANCH_LOCATION"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return branch;
    }

    @Override
    public List<Branch> getAll() {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = new Branch();
                branch.setId(rs.getInt("ID_BRANCH"));
                branch.setName(rs.getString("BRANCH_NAME"));
                branch.setPhone(rs.getString("BRANCH_PHONE"));
                branch.setLocation(rs.getString("BRANCH_LOCATION"));

                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Branch> getAll(String where, String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = Branch.fromRS(rs);
                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Branch> getAll(String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = Branch.fromRS(rs);
                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Branch save(Branch obj) {
        String sql = "INSERT INTO BRANCH(BRANCH_NAME, BRANCH_PHONE, BRANCH_LOCATION)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPhone());
            stmt.setString(3, obj.getLocation());
//            System.out.println(stmt);
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Branch update(Branch obj) {
        String sql = "UPDATE BRANCH"
                + " SET BRANCH_NAME = ? , BRANCH_PHONE = ?, BRANCH_LOCATION = ?"
                + " WHERE  ID_BRANCH = ?";
        Connection conn = DatabaseHelper.getConnect();  
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPhone());
            stmt.setString(3, obj.getLocation());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    

    @Override
    public int delete(Branch obj) {
        String sql = "DELETE FROM BRANCH WHERE ID_BRANCH=?" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
//            int ret = stmt.executeUpdate(sql);
//            return ret;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Branch> getAllOrderby(String name, String order) {
        ArrayList<Branch> list = new ArrayList();
        String sql = "SELECT * FROM BRANCH ORDER BY " + name + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Branch branch = new Branch();
                branch.setId(rs.getInt("ID_BRANCH"));
                branch.setName(rs.getString("BRANCH_NAME"));
                branch.setPhone(rs.getString("BRANCH_PHONE"));
                branch.setLocation(rs.getString("BRANCH_LOCATION"));

                list.add(branch);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
