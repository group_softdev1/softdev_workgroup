/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;


import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.WorkingHours;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class WorkingHoursDao implements Dao<WorkingHours> {

    @Override
    public WorkingHours get(int id) {
        WorkingHours workingHours = null;
        String sql = "SELECT * FROM WORKINGHOURS WHERE ID_WORKINGHOURS= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                workingHours = new WorkingHours();
                workingHours.setId(rs.getInt("ID_WORKINGHOURS"));
                workingHours.setIdEM(rs.getInt("ID_EM"));
                workingHours.setWTimeIn(rs.getString("WORKH_TIME_IN"));
                workingHours.setWTimeOut(rs.getString("WORKH_TIME_OUT"));
                workingHours.setTotal(rs.getInt("WORKH_TOTAL"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workingHours;
    }

    @Override
    public List<WorkingHours> getAll() {
        ArrayList<WorkingHours> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGHOURS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingHours workingHours = new WorkingHours();
                workingHours.setId(rs.getInt("ID_WORKINGHOURS"));
                workingHours.setIdEM(rs.getInt("ID_EM"));
                workingHours.setWTimeIn(rs.getString("WORKH_TIME_IN"));
                workingHours.setWTimeOut(rs.getString("WORKH_TIME_OUT"));
                workingHours.setTotal(rs.getInt("WORKH_TOTAL"));

                list.add(workingHours);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WorkingHours save(WorkingHours obj) {
        String sql = "INSERT INTO WORKINGHOURS(ID_EM, WORKH_TIME_IN, WORKH_TIME_OUT, WORKH_TOTAL)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getWTimeIn());
            stmt.setString(3, obj.getWTimeOut());
            stmt.setInt(4, obj.getTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public WorkingHours update(WorkingHours obj) {
        String sql = "UPDATE WORKINGHOURS"
                + " SET ID_EM = ? , WORKH_TIME_IN = ?, WORKH_TIME_OUT = ?, WORKH_TOTAL = ?"
                + " WHERE  ID_WORKINGHOURS = ?";
        Connection conn = DatabaseHelper.getConnect();  
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getWTimeIn());
            stmt.setString(3, obj.getWTimeOut());
            stmt.setInt(4, obj.getTotal());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(WorkingHours obj) {
        String sql = "DELETE FROM WORKINGHOURS WHERE ID_WORKINGHOURS=" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<WorkingHours> getAll(String where, String order) {
        ArrayList<WorkingHours> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGHOURS where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingHours workingHours = WorkingHours.fromRS(rs);
                list.add(workingHours);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<WorkingHours> getAll(String order) {
        ArrayList<WorkingHours> list = new ArrayList();
        String sql = "SELECT * FROM WORKINGHOURS  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkingHours workingHours = WorkingHours.fromRS(rs);
                list.add(workingHours);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
