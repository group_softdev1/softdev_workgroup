/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.DetailCheckStock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class DetailCheckStockDao implements Dao<DetailCheckStock> {

    @Override
    public DetailCheckStock get(int id) {
        DetailCheckStock detailCheckStock = null;
        String sql = "SELECT * FROM DETAILCHECKSTOCK WHERE ID_DCS= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                detailCheckStock = new DetailCheckStock();
                detailCheckStock.setId(rs.getInt("ID_DCS"));
                detailCheckStock.setIdCS(rs.getInt("ID_CS"));
                detailCheckStock.setIdIGD(rs.getInt("ID_IGD"));
                detailCheckStock.setName(rs.getString("DCS_NAME"));
                detailCheckStock.setQOriginal(rs.getInt("DCS_QUANTITY_ORIGINAL"));
                detailCheckStock.setQCurrent(rs.getInt("DCS_QUANTITY_CURRENT"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return detailCheckStock;
    }

    @Override
    public List<DetailCheckStock> getAll() {
        ArrayList<DetailCheckStock> list = new ArrayList();
        String sql = "SELECT * FROM DETAILCHECKSTOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailCheckStock detailCheckStock = new DetailCheckStock();
                detailCheckStock.setId(rs.getInt("ID_DCS"));
                detailCheckStock.setIdCS(rs.getInt("ID_CS"));
                detailCheckStock.setIdIGD(rs.getInt("ID_IGD"));
                detailCheckStock.setName(rs.getString("DCS_NAME"));
                detailCheckStock.setQOriginal(rs.getInt("DCS_QUANTITY_ORIGINAL"));
                detailCheckStock.setQCurrent(rs.getInt("DCS_QUANTITY_CURRENT"));

                list.add(detailCheckStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public DetailCheckStock save(DetailCheckStock obj) {
        String sql = "INSERT INTO DETAILCHECKSTOCK(ID_CS, ID_IGD, DCS_NAME, DCS_QUANTITY_ORIGINAL, DCS_QUANTITY_CURRENT)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdCS());
            stmt.setInt(2, obj.getIdIGD());
            stmt.setString(3, obj.getName());
            stmt.setInt(4, obj.getQOriginal());
            stmt.setInt(5, obj.getQCurrent()); 
//           System.out.println(stmt);
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public DetailCheckStock update(DetailCheckStock obj) {
        String sql = "UPDATE DETAILCHECKSTOCK"
                + " SET ID_CS = ? , ID_IGD = ?, DCS_NAME = ?, DCS_QUANTITY_ORIGINAL = ?, DCS_QUANTITY_CURRENT = ?"
                + " WHERE  ID_DCS = ?";
        Connection conn = DatabaseHelper.getConnect();  
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdCS());
            stmt.setInt(2, obj.getIdIGD());
            stmt.setString(3, obj.getName());
            stmt.setInt(4, obj.getQOriginal());
            stmt.setInt(5, obj.getQCurrent()); 
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(DetailCheckStock obj) {
        String sql = "DELETE FROM DETAILCHECKSTOCK WHERE ID_DCS=" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<DetailCheckStock> getAll(String where, String order) {
        ArrayList<DetailCheckStock> list = new ArrayList();
        String sql = "SELECT * FROM DETAILCHECKSTOCK where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailCheckStock detailCheckStock = DetailCheckStock.fromRS(rs);
                list.add(detailCheckStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<DetailCheckStock> getAll(String order) {
        ArrayList<DetailCheckStock> list = new ArrayList();
        String sql = "SELECT * FROM DETAILCHECKSTOCK  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailCheckStock detailCheckStock = DetailCheckStock.fromRS(rs);
                list.add(detailCheckStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<DetailCheckStock> getDetailCheckStockDao() {
        ArrayList<DetailCheckStock> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT DCS_NAME,DCS_QUANTITY_CURRENT FROM DETAILCHECKSTOCK ORDER BY DCS_QUANTITY_CURRENT DESC";
//                """
//                     SELECT DCS_NAME,DCS_QUANTITY_CURRENT 
//                     FROM DETAILCHECKSTOCK 
//                     ORDER BY DCS_QUANTITY_CURRENT DESC;
//                     """
                
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                
                DetailCheckStock detailCheckStock = new DetailCheckStock();
                
                detailCheckStock.setName(rs.getString("DCS_NAME"));
                detailCheckStock.setQCurrent(rs.getInt("DCS_QUANTITY_CURRENT"));

                list.add(detailCheckStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<DetailCheckStock> getAllSS() {
        ArrayList<DetailCheckStock> list = new ArrayList();
        String sql = """
                     SELECT DCS_NAME,DCS_QUANTITY_CURRENT 
                     FROM DETAILCHECKSTOCK 
                     ORDER BY DCS_QUANTITY_CURRENT DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailCheckStock detailCheckStock = DetailCheckStock.fromRS(rs);


                list.add(detailCheckStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
