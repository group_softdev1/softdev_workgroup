/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.BuyInGredients;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class BuyInGredientsDao implements Dao<BuyInGredients> {

    @Override
    public BuyInGredients get(int id) {
        BuyInGredients buyInGredients = null;
        String sql = "SELECT * FROM BUY_INGREDIENTS WHERE ID_DBI=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                buyInGredients = BuyInGredients.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return buyInGredients;
    }

    public List<BuyInGredients> getAll() {
        ArrayList<BuyInGredients> list = new ArrayList();
        String sql = "SELECT * FROM BUY_INGREDIENTS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BuyInGredients buyInGredients = BuyInGredients.fromRS(rs);
                list.add(buyInGredients);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<BuyInGredients> getAll(String where, String order) {
        ArrayList<BuyInGredients> list = new ArrayList();
        String sql = "SELECT * FROM BUY_INGREDIENTS WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BuyInGredients buyInGredients = BuyInGredients.fromRS(rs);
                list.add(buyInGredients);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BuyInGredients> getAll(String order) {
        ArrayList<BuyInGredients> list = new ArrayList();
        String sql = "SELECT * FROM BUY_INGREDIENTS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BuyInGredients buyInGredients = BuyInGredients.fromRS(rs);
                list.add(buyInGredients);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BuyInGredients save(BuyInGredients obj) {
        String sql = "INSERT INTO BUY_INGREDIENTS (ID_BI, ID_EM, ID_SEL, BI_ORDER_DATE, BI_RECEIEVE_DATE, BI_TOTAL_UNIT, BI_TOTAL_PRICE, BI_TOTAL_NET_AMOUNT)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdBI());
            stmt.setInt(2, obj.getIdEmployee());
            stmt.setInt(3, obj.getSel());
            stmt.setString(4, obj.getOderDate()); // ใช้ setTimestamp แทน setTimestamp
            stmt.setString(5, obj.getReceieveDate()); // ใช้ setTimestamp แทน setDate
            stmt.setInt(6, obj.getTotalUnit());
            stmt.setDouble(7, obj.getTotalPrice());
            stmt.setDouble(8, obj.getNetAmount());
             stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BuyInGredients update(BuyInGredients obj) {
        String sql = "UPDATE BUY_INGREDIENTS"
                + " SET ID_BI = ?, ID_EM = ?, ID_SEL = ?, BI_ORDER_DATE = ?, BI_RECEIEVE_DATE = ?, BI_TOTAL_UNIT = ?, BI_TOTAL_PRICE = ?, BI_TOTAL_NET_AMOUNT = ?"
                + " WHERE ID_DBI = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdBI());
            stmt.setInt(2, obj.getIdEmployee());
            stmt.setInt(3, obj.getSel());
            stmt.setString(4, obj.getOderDate()); // ใช้ setTimestamp แทน setTimestamp
            stmt.setString(5, obj.getReceieveDate()); // ใช้ setTimestamp แทน setDate
            stmt.setInt(6, obj.getTotalUnit());
            stmt.setDouble(7, obj.getTotalPrice());
            stmt.setDouble(8, obj.getNetAmount());
            stmt.setInt(9, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BuyInGredients obj) {
        String sql = "DELETE FROM BUY_INGREDIENTS WHERE ID_DBI = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
