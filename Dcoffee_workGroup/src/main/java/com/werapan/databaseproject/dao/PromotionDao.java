/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;

import com.werapan.databaseproject.model.Promotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class PromotionDao implements Dao<Promotion> {

    @Override
    public Promotion get(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM PROMOTION WHERE ID_PROM = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = new Promotion();
                promotion.setId(rs.getInt("ID_PROM"));
                promotion.setCondition(rs.getString("PROM_CONDITION"));
                promotion.setName(rs.getString("PROM_NAME"));
                promotion.setStatus(rs.getString("PROM_STATUS"));
                promotion.setStartDate(rs.getString("PROM_START_DATE"));
                promotion.setExpDate(rs.getString("PROM_EXP_DATE"));
              
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;

    }

//    public Promotion getByCondition(String condition) {
//        Promotion promotion = new Promotion();
//        String sql = "SELECT * FROM PROMOTION WHERE PROMOTION =?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, condition);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                promotion = promotion.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return promotion;
//    }
//    
    @Override
    public List<Promotion> getAll() {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = new Promotion();
                promotion.setId(rs.getInt("ID_PROM"));
                promotion.setCondition(rs.getString("PROM_CONDITION"));
                promotion.setName(rs.getString("PROM_NAME"));
                promotion.setStatus(rs.getString("PROM_STATUS"));
                promotion.setStartDate(rs.getString("PROM_START_DATE"));
                promotion.setExpDate(rs.getString("PROM_EXP_DATE"));

                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    @Override
    public List<Promotion> getAll(String where, String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Promotion> getAll(String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM PROMOTION ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Promotion save(Promotion obj) {

        String sql = "INSERT INTO PROMOTION (PROM_CONDITION, PROM_NAME, PROM_STATUS, PROM_START_DATE, PROM_EXP_DATE)" 
                + "VALUES (?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, obj.getCondition());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getStatus());
            stmt.setString(4, obj.getStartDate());
            stmt.setString(5, obj.getExpDate());
            
//            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
//            int id = DatabaseHelper.getInsertedId(stmt);
//            obj.setId(id);
            return obj ;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
//        return obj;
    }

    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE PROMOTION"
                + " SET PROM_CONDITION =? , PROM_NAME =  ?, PROM_STATUS =  ?, PROM_START_DATE =  ? ,PROM_EXP_DATE =  ?"
                + " WHERE  ID_PROM = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCondition());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getStatus());
            stmt.setString(4, obj.getStartDate());
            stmt.setString(5, obj.getExpDate());
            stmt.setInt(6, obj.getId());

            int ret = stmt.executeUpdate();
                System.out.println(ret);
//            return obj;
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM PROMOTION WHERE ID_PROM=" +obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
//            int ret = stmt.executeUpdate();
//            return ret;

            

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    
    }
}

//    public PromotionDao getByTel(String tel) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
//
//
