/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class MemberDao implements Dao<Member> {

    @Override
    public Member get(int id) {
        Member member = null;
        String sql = "SELECT * FROM MEMBER WHERE ID_MEMBER =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
              
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;

    }

    public Member getByTel(String tel) {
        Member member = null;
        String sql = "SELECT * FROM MEMBER WHERE MEMBER_TEL=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;
    }
    
    @Override
    public List<Member> getAll() {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    @Override
    public List<Member> getAll(String where, String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Member> getAll(String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Member save(Member obj) {

        String sql = "INSERT INTO MEMBER(MEMBER_FNAME_LNAME, MEMBER_DATE_REGISTER, MEMBER_POINT_MEMBER, MEMBER_TEL)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName()); 
            stmt.setString(2, obj.getDateRegister());
            stmt.setInt(3, obj.getPoint());
            stmt.setString(4, obj.getTel());
            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Member update(Member obj) {
        String sql = "UPDATE MEMBER"
                + " SET MEMBER_FNAME_LNAME = ? , MEMBER_DATE_REGISTER = ?, MEMBER_POINT_MEMBER = ?, MEMBER_TEL= ?"
                + " WHERE  ID_MEMBER = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName()); 
            stmt.setString(2, obj.getDateRegister());
            stmt.setInt(3, obj.getPoint());
            stmt.setString(4, obj.getTel());
            stmt.setInt(5, obj.getId());
            
            int ret = stmt.executeUpdate();
//            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    

    @Override
    public int delete(Member obj) {
        String sql = "DELETE  FROM MEMBER WHERE ID_MEMBER=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    
    }
}
