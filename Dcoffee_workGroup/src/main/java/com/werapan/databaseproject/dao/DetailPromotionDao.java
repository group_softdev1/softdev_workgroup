/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.DetailPromotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bbnpo
 */
public class DetailPromotionDao implements Dao<DetailPromotion> {

    @Override
    public DetailPromotion get(int id) {
        DetailPromotion detailPromotion = null;
        String sql = "SELECT * FROM  DETAIL PROMOTION WHERE ID_DEPROM=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                detailPromotion = DetailPromotion.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return detailPromotion;
    }

    @Override
    public List<DetailPromotion> getAll() {
        ArrayList<DetailPromotion> list = new ArrayList();
        String sql = "SELECT * FROM DETAIL PROMOTION ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailPromotion detailPromotion = DetailPromotion.fromRS(rs);
                list.add(detailPromotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<DetailPromotion> getAll(String where, String order) {
        ArrayList<DetailPromotion> list = new ArrayList();
        String sql = "SELECT * FROM DETAIL PROMOTION where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailPromotion detailPromotion = DetailPromotion.fromRS(rs);
                list.add(detailPromotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<DetailPromotion> getAll(String order) {
        ArrayList<DetailPromotion> list = new ArrayList();
        String sql = "SELECT * FROM DETAIL PROMOTION ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                DetailPromotion detailPromotion = DetailPromotion.fromRS(rs);
                list.add(detailPromotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public DetailPromotion save(DetailPromotion obj) {
        String sql = "INSERT INTO DETAIL PROMOTION(ID_DEPROM, ID_MENU, ID_PROM, DEPROM_DETAIL, DEPROM_DISCOUNT)"
                + "VALUES(?, ?, ?, ? ,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getIdMenu());
            stmt.setInt(3, obj.getIdProm());
            stmt.setString(4, obj.getDepromDetail());
            stmt.setInt(5, obj.getDiscount());

            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public DetailPromotion update(DetailPromotion obj) {
        String sql = "UPDATE DETAIL PROMOTION"
                + " SET ID_MENU = ?, ID_PROM = ?, DEPROM_DETAIL = ?, DEPROM_DISCOUNT = ?"
                + " WHERE ID_DEPROM = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getIdMenu());
            stmt.setInt(3, obj.getIdProm());
            stmt.setString(4, obj.getDepromDetail());
            stmt.setInt(5, obj.getDiscount());

            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public int delete(DetailPromotion obj) {
        String sql = "DELETE FROM  DETAIL PROMOTION WHERE ID_DEPROM=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

}
