/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Bill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BillDao implements Dao<Bill> {

    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM BILL WHERE ID_BILL =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                bill = Bill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Bill save(Bill obj) {
        String sql = "INSERT INTO BILL(ID_EM, LIST, START_DATE, END_DATE, THISNUMBER, PERNUMBER, UNIT, PERUNIT, PRICE, TOTALPRICE)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getList());
            stmt.setString(3, obj.getStartDate());
            stmt.setString(4, obj.getEndDate());
            stmt.setDouble(5, obj.getThisNumber());
            stmt.setDouble(6, obj.getPernumber());
            stmt.setDouble(7, obj.getUnit());
            stmt.setDouble(8, obj.getPerUnit());
            stmt.setDouble(9, obj.getPrice());
            stmt.setDouble(10, obj.getTotalPrice());
            
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE BILL"
                + " SET ID_EM = ? , LIST = ?, START_DATE = ?, END_DATE = ?, THISNUMBER = ?, PERNUMBER = ?, UNIT = ?, PERUNIT = ?, PRICE = ?, TOTALPRICE = ?"
                + " WHERE  ID_BILL = ?";
        Connection conn = DatabaseHelper.getConnect();  
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIdEM());
            stmt.setString(2, obj.getList());
            stmt.setString(3, obj.getStartDate());
            stmt.setString(4, obj.getEndDate());
            stmt.setDouble(5, obj.getThisNumber());
            stmt.setDouble(6, obj.getPernumber());
            stmt.setDouble(7, obj.getUnit());
            stmt.setDouble(8, obj.getPerUnit());
            stmt.setDouble(9, obj.getPrice());
            stmt.setDouble(10, obj.getTotalPrice());
            stmt.setInt(11, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM BILL WHERE ID_BILL=" + obj.getId();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
