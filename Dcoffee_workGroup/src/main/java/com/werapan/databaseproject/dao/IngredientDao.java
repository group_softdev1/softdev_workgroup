/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Ingredient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class IngredientDao implements Dao<Ingredient> {

    @Override
    public Ingredient get(int id) {
        Ingredient product = null;
        String sql = "SELECT * FROM INGREDIENT WHERE ID_IGD=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }

    @Override
    public List<Ingredient> getAll() {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient product = Ingredient.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Ingredient> getAll(String where, String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient product = Ingredient.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Ingredient> getAll(String order) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM INGREDIENT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient product = Ingredient.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ingredient save(Ingredient obj) {

        String sql = "INSERT INTO INGREDIENT (IGD_NAME ,IGD_AMOUNT , IGD_PRICE)"
                + "VALUES( ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
          //  stmt.setInt(6, obj.getCategoryId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE INGREDIENT"
                + " SET IGD_NAME = ?, IGD_AMOUNT = ?, IGD_PRICE = ?"
                + " WHERE ID_IGD = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(4, obj.getId());
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredient obj) {
        String sql = "DELETE FROM INGREDIENT WHERE ID_IGD=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
