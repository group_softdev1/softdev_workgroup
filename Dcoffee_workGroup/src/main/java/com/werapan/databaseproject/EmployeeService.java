/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject;

import com.werapan.databaseproject.dao.EmployeeDao;
import com.werapan.databaseproject.model.Employee;
import java.util.List;

/**
 *
 * @author werapan
 */
public class EmployeeService {
    public Employee login (String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if(employee != null && employee.getPassword().equals(password)) {
            return employee;
        }
        return null;
    }
    
    public List<Employee> getUsers(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" EM_LOGIN asc");
    }
    
    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
}
