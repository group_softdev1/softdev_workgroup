/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DatabaseHelper.dao;

import DatabaseHelper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modal.Member;

/**
 *
 * @author bbnpo
 */
public class MemberDao implements Dao<Member> {

    @Override
    public Member get(int id) {
        Member member = null;
        String sql = "SELECT * FROM MEMBER WHERE ID_MEMBER =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
              
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;

    }

    public Member getByTel(String tel) {
        Member member = null;
        String sql = "SELECT * FROM MEMBER WHERE MEMBER_TEL=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;
    }
    
    @Override
    public List<Member> getAll() {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    @Override
    public List<Member> getAll(String where, String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Member> getAll(String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM MEMBER ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Member save(Member obj) {

        String sql = "INSERT INTO MEMBER(ID_MEMBER, MEMBER_FNAME_LNAME, MEMBER_ADDRESS, MEMBER_DATE_OF_BIRTH, MEMBER_DATE_REGISTER, MEMBER_POINT_MEMBER, MEMBER_TEL)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getAddress());
            stmt.setString(4, obj.getBirth());
            stmt.setString(5, obj.getDateRegister());
            stmt.setInt(6, obj.getPoint());
            stmt.setString(7, obj.getTel());
            int ret = stmt.executeUpdate();

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Member update(Member obj) {
        String sql = "UPDATE MEMBER"
                + "SET MEMBER_FNAME_LNAME =  ? , MEMBER_ADDRESS =  ?,MEMBER_DATE_OF_BIRTH =  ?,MEMBER_DATE_REGISTER =  ?, MEMBER_POINT_MEMBER =  ?,MEMBER_TEL=  ?"
                + "WHERE  ID_MEMBER = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getAddress());
            stmt.setString(4, obj.getBirth());
            stmt.setString(5, obj.getDateRegister());
            stmt.setInt(6, obj.getPoint());
            stmt.setString(7, obj.getTel());
           int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    

    @Override
    public int delete(Member obj) {
        String sql = "SELECT * FROM MEMBER WHERE ID_MEMBER =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

            

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    
    }
}