/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import DatabaseHelper.dao.MemberDao;
import java.util.List;
import modal.Member;

/**
 *
 * @author werapan
 */
public class MemberService {
    public Member getByTel(String tel) {
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.getByTel(tel);
        return member;
        }
    
    public List<Member> getMembers(){
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll("MEMBER_FNAME_LNAME asc");
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);    
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);     
    }
}
